package dao.conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexao {

    private static String url = "jdbc:postgresql://200.18.128.54/aula";
    private static String usuario = "aula";
    private static String senha = "aula";

    private static Connection con;

    public static Connection getConexao(){
        try {
            if(Conexao.con==null || Conexao.con.isClosed()){
                System.out.println("Tentando se conectar. Usuario: "+usuario+ " - senha: " + senha + " url: " + url + "");
                Conexao.con = DriverManager.getConnection(url, usuario, senha);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            Logger.getAnonymousLogger().log(Level.INFO,e.getMessage());
            e.printStackTrace();
            return null;
        }
        return Conexao.con;
    }
}
