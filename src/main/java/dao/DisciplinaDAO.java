package dao;

import dao.conexao.Conexao;
import model.Disciplina;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DisciplinaDAO {

    public int inserirDisciplina(Disciplina inserir){
        try {
            String SQL = "INSERT INTO  janio_silva.disciplina(nome_disciplina, ementa, ch, id_curso) VALUES (?,?,?,?);";
            int retorno =0;
            Connection c = Conexao.getConexao();
            PreparedStatement ps = c.prepareStatement(SQL);
            ps.setString(1,inserir.getNome_disciplina());
            ps.setString(2,inserir.getEmenta());
            ps.setInt(3,Integer.valueOf(inserir.getCh()));
            ps.setInt(4,inserir.getIdCurso());
            retorno= ps.executeUpdate();

            return retorno;
        }catch(SQLException ex){
            Logger.getAnonymousLogger().log(Level.INFO, ex.getMessage());
            return 0;
        }
    }

    public List<Disciplina> listarTodasDisciplinas(){
        try {
            String SQL = "SELECT * FROM janio_silva.disciplina;";
            List<Disciplina> retorno = new ArrayList<Disciplina>();
            Connection c = Conexao.getConexao();
            PreparedStatement ps = c.prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Disciplina atual = this.formataDisciplina(rs);
                retorno.add(atual);
            }
            return retorno;
        }catch(SQLException ex){
            Logger.getAnonymousLogger().log(Level.INFO, ex.getMessage());
            return null;
        }
    }
    private Disciplina formataDisciplina(ResultSet rs){
        try {
            Disciplina atual = new Disciplina();
            atual.setId_disciplina(rs.getInt("id_disciplina"));
            atual.setNome_disciplina(rs.getString("nome_disciplina"));
            atual.setCh(rs.getString("ch"));
            atual.setEmenta(rs.getString("ementa"));
            return atual;
        }catch(SQLException ex){
            Logger.getAnonymousLogger().log(Level.INFO, ex.getMessage());
            return null;
        }
    }
}
