package dao;

import dao.conexao.Conexao;
import model.Turma;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TurmaDAO {

    public int inserirTurma(Turma inserir){
        try {
            String SQL = "INSERT INTO janio_silva.TURMA (ano, semestre, cpf, id_disciplina, curso ) VALUES (?,?,?,?,?);";
            Connection c = Conexao.getConexao();
            PreparedStatement ps = c.prepareStatement(SQL);
            ps.setInt(1, inserir.getAno());
            ps.setInt(2, inserir.getSemestre());
            ps.setString(3, inserir.getCpf());
            ps.setInt(4, inserir.getIdDisciplina());
            ps.setInt(5, inserir.getIdCurso());
            int retorno = ps.executeUpdate();
            return retorno;
        }catch (SQLException ex){
            Logger.getAnonymousLogger().log(Level.INFO, ex.getMessage());
            return 0;
        }
    }

    public List<Turma> listarTodasTurmas(){
        try {
            List<Turma> retorno = new ArrayList<Turma>();
            String SQL = "SELECT * FROM janio_silva.turma";
            Connection c = Conexao.getConexao();
            PreparedStatement ps = c.prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Turma atual = formataTurma(rs);
                retorno.add(atual);
            }
            return retorno;
        }catch(SQLException ex){
            Logger.getAnonymousLogger().log(Level.INFO, ex.getMessage());
            return null;
        }
    }

    private Turma formataTurma(ResultSet rs) {
        Turma atual = new Turma();
        try {
            atual.setAno(rs.getInt("ano"));
            atual.setCpf(rs.getString("cpf"));
            atual.setIdTurma(rs.getInt("id_turma"));
            atual.setDataCadastro(rs.getString("data_cadastro"));
            atual.setSemestre(rs.getInt("semestre"));
            atual.setIdCurso(rs.getInt("curso"));
            atual.setIdDisciplina(rs.getInt("id_disciplina"));
            return atual;
        }catch (SQLException ex){
            Logger.getAnonymousLogger().log(Level.INFO, ex.getMessage());
            return null;
        }
    }
}
