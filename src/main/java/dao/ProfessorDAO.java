package dao;

import dao.conexao.Conexao;
import model.Professor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProfessorDAO {

    public int inserirProfessor(Professor inserir){
        try {
            String instrucaoSQL = "INSERT INTO janio_silva.professor (cpf, nome_professor, email, telefone) VALUES (?,?,?,?);";
            Connection c = Conexao.getConexao();
            PreparedStatement ps = c.prepareStatement(instrucaoSQL);
            ps.setString(1, inserir.getCpf());
            ps.setString(2, inserir.getNome_professor());
            ps.setString(3, inserir.getEmail());
            ps.setString(4, inserir.getTelefone());
            int retorno = ps.executeUpdate();
            return retorno;
        }
        catch (SQLException erro){
            Logger.getAnonymousLogger().log(Level.INFO, erro.getMessage());
            return 0;
        }
    }

    public List<Professor> listarTodosProfessores(){
        try {
        List<Professor> retorno = new ArrayList<Professor>();
        String SQL = "SELECT * FROM janio_silva.professor";
        Connection c = Conexao.getConexao();
        PreparedStatement ps =c.prepareStatement(SQL);
        ResultSet resultado=ps.executeQuery();
        while(resultado.next()){
            Professor atual = this.formataProfessor(resultado);
            retorno.add(atual);
        }
        return retorno;
        }catch(SQLException ex){
            Logger.getAnonymousLogger().log(Level.INFO, ex.getMessage());
            return null;
        }
    }

    private Professor formataProfessor(ResultSet resultado){
        try {
            Professor atual = new Professor();
            atual.setCpf(resultado.getString("cpf"));
            atual.setNome_professor(resultado.getString("nome_professor"));
            atual.setEmail(resultado.getString("email"));
            atual.setTelefone(resultado.getString("telefone"));
            return atual;
        }catch(SQLException ex){
            Logger.getAnonymousLogger().log(Level.INFO, ex.getMessage());
            return null;
        }

    }






}
