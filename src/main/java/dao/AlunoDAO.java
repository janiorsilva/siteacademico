package dao;

import dao.conexao.Conexao;
import model.Aluno;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AlunoDAO {

    public List<Aluno> getAlunos() {
        List<Aluno> retorno = new ArrayList<Aluno>();
        String SQL = "select * from janio_silva.aluno;";
        try {
            Connection c = Conexao.getConexao();
            PreparedStatement ps = c.prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();
            System.out.println(SQL + " / " + rs);
            while (rs.next()) {
                Aluno atual = this.formataAluno(rs);
                retorno.add(atual);
            }
            c.close();
        } catch (SQLException ex) {
            Logger.getAnonymousLogger().log(Level.INFO, ex.getMessage());
            System.out.println(ex.getMessage());
        }
        return retorno;
    }

    private Aluno formataAluno(ResultSet rs) {
        Aluno a = new Aluno();
        try {

            a.setMatricula(rs.getString("matricula"));
            a.setEmail(rs.getString("email"));
            a.setNome(rs.getString("nome_aluno"));
            a.setTelefone(rs.getString("telefone"));
        } catch (SQLException ex) {
            Logger.getAnonymousLogger().log(Level.INFO, ex.getMessage());
            System.out.println(ex.getMessage());
        }
        return a;
    }

    public int insereAluno(Aluno a) {
        try {
            String SQL = "INSERT INTO janio_silva.aluno (matricula, email, nome_aluno,telefone ) VALUES (?,?,?,?);";
            Logger.getAnonymousLogger().log(Level.INFO,"VERIFICAR: "+ a.toString());
            Connection c = Conexao.getConexao();
            PreparedStatement ps = c.prepareStatement(SQL);
            ps.setInt(1, Integer.valueOf( a.getMatricula() ) );
            ps.setString(2,a.getEmail());
            ps.setString(3,a.getNome());
            ps.setString(4,a.getTelefone());
            int resultado = ps.executeUpdate();
            c.close();
            return resultado;
        } catch (SQLException ex) {
            Logger.getAnonymousLogger().log(Level.INFO, ex.getMessage());
            System.out.println(ex.getMessage());
            return 0;
        }
    }
}
