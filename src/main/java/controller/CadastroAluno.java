package controller;

import dao.AlunoDAO;
import jakarta.ejb.Stateful;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import model.Aluno;
import model.Disciplina;
import model.Professor;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named @SessionScoped @Stateful(passivationCapable=true)
public class CadastroAluno  {

    private String nome="";
    private String email="";
    private String telefone="";
    private String matricula="";

    private String mensagem="Bem vindo ao cadastro de alunos!";
    private Aluno aluno =null;

    public CadastroAluno() {

    }

    public void testar() {

    }
    public CadastroAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public boolean gravarDados() {
        Aluno a = new Aluno(this.aluno.getMatricula(),this.aluno.getNome(),
                this.aluno.getEmail(),this.aluno.getTelefone());
        return true;
    }

    public List<Aluno> listarAlunos(){
        AlunoDAO ad = new AlunoDAO();
        return  ad.getAlunos();
    }

    public void insereAluno(){

        AlunoDAO ad = new AlunoDAO();
        this.aluno = new Aluno(this.matricula,this.nome,this.telefone,this.email);
        this.mensagem="";
        Logger.getAnonymousLogger().log(Level.INFO,"Tentando cadastrar o aluno!");
        int ret = ad.insereAluno(this.aluno);
        if(ret>0){
            this.mensagem="Aluno Inserido com SUCEESSO!";
        }else{
            this.mensagem="Erro ao inserir aluno! Verifique!";
        }
        //this.mensagem="Aluno: "+ this.getNome() + " - "+ this.getMatricula()+" - "+this.getEmail()+" - "+this.getTelefone();
    }
    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
}
