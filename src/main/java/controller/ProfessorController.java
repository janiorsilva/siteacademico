package controller;

import dao.ProfessorDAO;
import jakarta.ejb.Stateful;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import model.Professor;

import java.util.List;

@Named @SessionScoped @Stateful(passivationCapable=true)
public class ProfessorController {

    private Professor profAtual = new Professor();
    private String mensagem;

    public ProfessorController(Professor novoProf) {
        this.profAtual = novoProf;
    }

    public ProfessorController() {
    }

    public Professor getProfAtual() {
        return profAtual;
    }

    public void setProfAtual(Professor profAtual) {
        this.profAtual = profAtual;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public void inserirProfessor(){
        ProfessorDAO pd = new ProfessorDAO();
        int ret  = pd.inserirProfessor(this.profAtual);
        if(ret>0){
            this.mensagem="Professor INSERIDO COM SUCESSO!";
        }else{
            this.mensagem="ERRO ao inserir Professor!";
        }
    }

    public List<Professor> listarProfessores(){
        ProfessorDAO pd = new ProfessorDAO();
        return pd.listarTodosProfessores();
    }
}
