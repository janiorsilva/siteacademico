package controller;

import dao.TurmaDAO;
import jakarta.ejb.Stateful;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import model.Turma;

import java.util.List;

@Named @SessionScoped @Stateful(passivationCapable=true)
public class TurmaController {

    private String mensagem="Bem vindo(a) ao Cadastro de Turmas!!";
    private Turma turmaAtual = new Turma();

    public TurmaController() {
    }

    public  void inserirTurma(){
        TurmaDAO td = new TurmaDAO();
        int retorno=td.inserirTurma(this.turmaAtual);
        if(retorno>0){
            mensagem="Turma INSERIDA com SUCESSO!!";
        }else{
            mensagem="ERRO ao inserir a Turma! Verifique!";
        }
    }

    public List<Turma> listarTodasTurmas(){
        TurmaDAO td = new TurmaDAO();
        return td.listarTodasTurmas();
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public Turma getTurmaAtual() {
        return turmaAtual;
    }

    public void setTurmaAtual(Turma turmaAtual) {
        this.turmaAtual = turmaAtual;
    }
}
