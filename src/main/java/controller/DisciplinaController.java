package controller;

import dao.DisciplinaDAO;
import jakarta.ejb.Stateful;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import model.Disciplina;

import java.util.List;

@Named @SessionScoped @Stateful(passivationCapable=true)
public class DisciplinaController {

    private String mensagem="Sejam Bem Vindos ao Cadastro de DISCIPLINAS!";
    private Disciplina disciplinaAtual = new Disciplina();

    public DisciplinaController(String mensagem, Disciplina disciplinaAtual) {
        this.mensagem = mensagem;
        this.disciplinaAtual = disciplinaAtual;
    }

    public DisciplinaController() {

    }

    public void inserirDisciplina(){
        DisciplinaDAO dd = new DisciplinaDAO();
        int retorno=dd.inserirDisciplina(disciplinaAtual);
        if(retorno>0){
            this.mensagem="Disciplina inserida com sucesso!";
        }else{
            this.mensagem="ERRO ao inserir disciplina!";
        }
    }

    public List<Disciplina> listarTodasDisicplinas(){
        DisciplinaDAO dd = new DisciplinaDAO();
        return  dd.listarTodasDisciplinas();
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public Disciplina getDisciplinaAtual() {
        return disciplinaAtual;
    }

    public void setDisciplinaAtual(Disciplina disciplinaAtual) {
        this.disciplinaAtual = disciplinaAtual;
    }
}
