package model;

public class Professor {
    private String cpf;
    private String nome_professor;
    private String email;
    private String telefone;

    public Professor() {
    }

    public Professor(String nCpf, String nNome_professor, String nEmail, String nTelefone) {
        this.cpf = nCpf;
        this.nome_professor = nNome_professor;
        this.email = nEmail;
        this.telefone = nTelefone;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome_professor() {
        return nome_professor;
    }

    public void setNome_professor(String nome_professor) {
        this.nome_professor = nome_professor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
