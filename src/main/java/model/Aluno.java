package model;


import java.io.Serializable;

public class Aluno implements Serializable {

    private String matricula;
    private String nome;
    private String telefone;
    private String email;

    public Aluno() {
    }

    public Aluno(String nMatricula, String nNome, String nTelefone, String nEmail) {
        this.matricula = nMatricula;
        this.nome = nNome;
        this.telefone = nTelefone;
        this.email = nEmail;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "{" +
                "='" + matricula + '\'' +
                ", ='" + nome + '\'' +
                ", ='" + telefone + '\'' +
                ", ='" + email + '\'' +
                '}';
    }
}
